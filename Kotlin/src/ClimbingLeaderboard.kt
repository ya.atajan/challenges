import java.util.*
import kotlin.collections.*
import kotlin.io.*
import kotlin.text.*

// Complete the climbingLeaderboard function below.
fun climbingLeaderboard(scores: Array<Int>, alice: Array<Int>, acount: Int, scount: Int): Array<Int> {
    var table: HashMap<Int, Int> = HashMap()
    var newAr: Array<Int> = Array(acount) {0}
    var place = 1
    var record = 1

    for (i in 0 until scount) {
        if (!table.containsValue(scores[i])) {
            table[scores[i]] = place
            place++
        }
    }

    print(table.entries)

    for (i in 0 until acount) {
        if (table.containsValue(alice[i])) {
            newAr[i] = table[alice[i]]!!
        } else {
            for (key in table.keys) {
                if (alice[i] > key) {
                    newAr[i] = record
                    record = 0
                }
                record++
            }
        }
    }

    print(Arrays.toString(newAr))
    return newAr
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val scoresCount = scan.nextLine().trim().toInt()

    val scores = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    val aliceCount = scan.nextLine().trim().toInt()

    val alice = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    val result = climbingLeaderboard(scores, alice, aliceCount, scoresCount)

    println(result.joinToString("\n"))
}
