import java.util.*
import kotlin.io.*
import kotlin.ranges.*
import kotlin.text.*

// Complete the countingValleys function below.
fun countingValleys(n: Int, s: String): Int {
    var inValley = false
    var altitude = 0
    var valley = 0

    for (i in 0 until n) {
        if (s[i] == 'U') {
            altitude++
            if (inValley && altitude >= 0) {
                inValley = false
            }
        } else {
            altitude--
            if (!inValley && altitude < 0) {
                inValley = true
                valley++
            }
        }
    }

    return valley
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val s = scan.nextLine()

    val result = countingValleys(n, s)

    println(result)
}
