import java.util.*
import kotlin.io.*
import kotlin.ranges.*
import kotlin.text.*

// Complete the repeatedString function below.
fun repeatedString(s: String, n: Long): Long {
    var count: Long = 0
    if (s.contains('a')) {
        var mul = n / s.length
        var rem = n % s.length

        for (chr in s) {
            if (chr == 'a') {
                count++
            }
        }

        count = (count * mul)

        for (i in 0 until rem) {
            if (s[i.toInt()] == 'a') {
                count++
            }
        }
    } else {
        return count
    }
    return count
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val s = scan.nextLine()

    val n = scan.nextLine().trim().toLong()

    val result = repeatedString(s, n)

    println(result)
}
