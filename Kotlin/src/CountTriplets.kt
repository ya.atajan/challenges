import java.util.*
import kotlin.collections.*
import kotlin.collections.HashSet
import kotlin.io.*
import kotlin.ranges.*
import kotlin.text.*

// Complete the countTriplets function below.
// O(n(log n))
fun countTriplets(arr: Array<Long>, r: Long): Long {
    var leftMap: HashMap<Long, Int> = HashMap()
    var count = 0

    for (i in 1 until arr.size - 1) {
        if (leftMap.containsKey(arr[i - 1])) {
            leftMap[arr[i - 1]] = leftMap.getValue(arr[i - 1]) + 1
        } else {
            leftMap[arr[i - 1]] = 1
        }

        var rhs = 0
        for (j in i + 1 until arr.size) {
            if (arr[j] == arr[i] * r) {
                rhs++
            }
        }

        if (leftMap.get(arr[i]/r) != null) {
            count += (leftMap.get(arr[i]/r))!!.toInt() * rhs
        }
    }

    return count.toLong()
}

// Complete the countTriplets function below.
// TODO: fix this O(n)
fun countTriplets2(arr: Array<Long>, r: Long): Long {
    var leftMap: HashMap<Long, Int> = HashMap()
    var rightMap: HashMap<Long, Int> = HashMap()
    var count = 0

    for (i in 1 until arr.size - 1) {
        if (leftMap.containsKey(arr[i - 1])) {
            leftMap[arr[i - 1]] = leftMap.getValue(arr[i - 1]) + 1
        } else {
            leftMap[arr[i - 1]] = 1
        }

        for (j in i + 1 until arr.size) {
            if (rightMap.containsKey(arr[i - 1])) {
                rightMap[arr[j]] = rightMap.getValue(arr[j]) + 1
            } else {
                rightMap[arr[j]] = 1
            }
        }

        var rhs = rightMap[arr[i] * r]

        if (leftMap[arr[i]/r] != null) {
            count += (leftMap[arr[i]/r])!!.toInt() * rhs!!.toInt()
        }
    }

    return count.toLong()
}

fun main(args: Array<String>) {
    val nr = readLine()!!.trimEnd().split(" ")

    val n = nr[0].toInt()

    val r = nr[1].toLong()

    val arr = readLine()!!.trimEnd().split(" ").map{ it.toLong() }.toTypedArray()

    val ans = countTriplets(arr, r)

    println(ans)
}