import java.util.*

fun IntRange.random() =
    Random().nextInt((endInclusive + 1) - start) +  start

fun main(args: Array<String>) {
    var card = (1..21).random()

    val (a, b) = readLine()!!.split(' ')

    val pick: Int = a.toInt() + b.toInt()

    if (pick in (card + 1)..21) {
        print(card)
        print("you win")
    } else {
        print(card)
        print("try again")
    }
}