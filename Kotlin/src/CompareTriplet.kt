import kotlin.collections.*
import kotlin.io.*
import kotlin.ranges.*
import kotlin.text.*

// Complete the compareTriplets function below.
fun compareTriplets(a: Array<Int>, b: Array<Int>): Array<Int> {
    var ar: Array<Int> = arrayOf(0, 0)

    for (i in 0 until a.size) {
        if (a[i] > b[i]) {
            ar[0] = ar[0] + 1
        } else if (a[i] < b[i]) {
            ar[1] = ar[1] + 1
        }
    }

    return ar
}

fun main(args: Array<String>) {
    val a = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

    val b = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

    val result = compareTriplets(a, b)

    println(result.joinToString(" "))
}
