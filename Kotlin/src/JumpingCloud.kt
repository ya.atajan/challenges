import java.io.*
import java.math.*
import java.security.*
import java.text.*
import java.util.*
import java.util.concurrent.*
import java.util.function.*
import java.util.regex.*
import java.util.stream.*
import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.jvm.*
import kotlin.jvm.functions.*
import kotlin.jvm.internal.*
import kotlin.ranges.*
import kotlin.sequences.*
import kotlin.text.*

// Complete the jumpingOnClouds function below.
fun jumpingOnClouds(c: Array<Int>): Int {
    for (i in 0 until c.size) {
        //c[i+2] != 1 ? (res++,i++) : (res++,i+=2)
    }
    return 0
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val c = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    println(jumpingOnClouds(c))
}
