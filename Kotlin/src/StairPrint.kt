import java.util.*
import kotlin.ranges.*
import kotlin.text.*

// Complete the staircase function below.
fun staircase(n: Int) {
    var s = n - 1

    while (s >= 0) {
        var h = n - s

        for (i in 0 until s) {
            print(" ")
        }

        for (i in 0 until h) {
            print("#")
        }
        s -= 1
        println()
    }
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    staircase(n)
}