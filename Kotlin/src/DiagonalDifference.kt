import java.util.*
import kotlin.collections.*
import kotlin.io.*
import kotlin.ranges.*
import kotlin.text.*

// Complete the diagonalDifference function below.
fun diagonalDifference(arr: Array<Array<Int>>, length: Int): Int {
    var prim = 0
    var sec = 0

    for (i in  0 until length) {
        prim += arr[i][i]
        sec += arr[i][length-1 - i]
    }

    return Math.abs(prim - sec)
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val arr = Array<Array<Int>>(n, { Array<Int>(n, { 0 }) })

    for (i in 0 until n) {
        arr[i] = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()
    }

    val result = diagonalDifference(arr, n)

    println(result)
}