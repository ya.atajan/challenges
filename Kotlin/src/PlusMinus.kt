import java.io.*
import java.math.*
import java.security.*
import java.text.*
import java.util.*
import java.util.concurrent.*
import java.util.function.*
import java.util.regex.*
import java.util.stream.*
import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.jvm.*
import kotlin.jvm.functions.*
import kotlin.jvm.internal.*
import kotlin.ranges.*
import kotlin.sequences.*
import kotlin.text.*

// Complete the plusMinus function below.
fun plusMinus(arr: Array<Int>) {
    var p = 0.0
    var n = 0.0
    var z = 0.0

    for (i in 0 until arr.size) {
        if(arr[i] > 0) {
            p++
        } else if (arr[i] < 0) {
            n++
        } else {
            z++
        }
    }

    val sum = p + n + z

    println("%.6f".format(p/sum))
    println("%.6f".format(n/sum))
    println("%.6f".format(z/sum))
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val arr = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    plusMinus(arr)
}
