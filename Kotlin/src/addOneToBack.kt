import java.util.*
import kotlin.collections.*
import kotlin.io.*
import kotlin.text.*

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val ar = scan.nextLine().split(" ").map { it.trim().toInt() }.toIntArray()
    val size = n - 1
    var carry = 1

    for (i in size downTo 0) {
        if (ar[i] + carry == 10 && i == 0) {
            var newAr = IntArray(n + 1)
            newAr[0] = 1
            println(Arrays.toString(newAr))
            break
        }else if (ar[i] + carry == 10) {
            ar[i] = (ar[i] + carry) % 10
        } else if (ar[i] + carry != 10) {
            ar[i] += carry
            println(Arrays.toString(ar))
            break
        }
    }
}