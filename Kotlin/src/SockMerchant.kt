import java.util.*
import kotlin.collections.*
import kotlin.io.*
import kotlin.text.*
import java.util.HashSet



// Complete the sockMerchant function below.
fun sockMerchant(n: Int, ar: Array<Int>): Int {
    val socks = HashSet<Int>()
    var match = 0

    for (i in 0 until n) {
        if (!socks.contains(ar[i])) {
            socks.add(ar[i])
        } else {
            match++
            socks.remove(ar[i])
        }
    }

    return match
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val ar = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

    val result = sockMerchant(n, ar)

    println(result)
}
